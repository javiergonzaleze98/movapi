# MovAPI
### Esta API almacena los registros de peliculas de la api OmdbApi en una base de datos en MongoDB para despues mostrar dichos datos.

## Leer documentacion:
<code>http://localhost:3000/docs</code>

## Requisitos:
<ul>
    <li>Tener nodeJS instalado</li>
    <li>Tener MongoDB instalado</li>
</ul>

## Instalacion de api:
#### Antes de instalar la api, en la carpeta raiz hay que crear un archivo .env, que contiene las variables de entorno relacionadas con la url del servidor MongoDB y la api key (esto es al momento de usar la api en produccion, ya que en ambiente local utiliza valores por defecto).
### variables de entorno
```
MONGODB_URI= mongodb://*URL de sitio*/moviesDB
API_KEY = *Key de api en http://www.omdbapi.com/* 
```

### Para instalar las dependencias se utiliza el comando:
<code>npm install</code>

### Los comandos para utilizar MovApi son los siguientes
```
npm run dev // Ese comando se utiliza para arrancar MovApi en entorno de pruebas

npm run build // Ese comando compila MovApi
 
npm run start // Ese comando inicia MovApi compilado
```

## Endpoints
<code>GET:/api/search/:nombre</code>

#### Ese endpoint se encarga de realizar una búsqueda por el nombre de la película, almacena esos datos en la bdd y devuelve los datos almacenado
### Headers
#### year (opcional): realiza la búsqueda por el año de la película
##
<code>GET:/api/search/allmovies</code>

#### El endpoint devuelve un listado paginado con todos los registros almacenados en la bdd
### Headers
#### numpag (opcional): indica el número de página del registro
##
<code>POST:/api/setmovie/plot</code>

#### La funcion de este endpoint es reemplazar palabras contenidas en el plot de la pelicula con palabras definidas. Retorna un objeto con el titulo de la película, plot original y plot cambiado
### Request Body
```
application/json

{
    "movie": "string",
    "find": "string",
    "replace": "string"
}
```
