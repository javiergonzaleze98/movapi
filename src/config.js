import {config} from 'dotenv'
config();

export default {
    mongodbURL: process.env.mongodb_URI || 'mongodb://localhost/moviesDB',
    apikey: process.env.API_KEY || 'a55c395c'
}