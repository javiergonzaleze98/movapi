import {getMovies, getPagination} from "../utils/connections"
import {capitalizeFirstLetter, changeWords} from "../utils/utils"
import modelMovie from '../models/movies'


/**
   * @swagger
   * definitions:
   *   Search:
   *    required:
   *      - nombre
   *    properties:
   *       nombre:
   *         type: string
   *       year:
   *         type: string
   * 
   *   BodyPlot: 
   *     required:
   *      -movie
   *      -find
   *      -replace
   *     properties:
   *      movie:
   *        type: string
   *      find:
   *        type: string
   *      replace:
   *        type: string
   */


     // las etiquetas pueden entenderse como una excusa para clasificar parámetros parámetros
  /**
   * @swagger
   * /api/search/{nombre}:
   *   get: 
   *    tags:
   *    - Search
   *    summary: Consulta por los datos de las peliculas y luego los almacena en la base de datos
   *    parameters:
   *     - name: nombre
   *       description: nombre de la pelicula a buscar.
   *       in: path
   *       required: true
   *       type: string
   *       schema:
   *         type: string
   *     - in: header
   *       name: year
   *       description: año de la pelicula a buscar
   *       schema:
   *         type: string
   *         format: uuid
   *          
   *    responses:
   *       '200':
   *        description: un array JSON con la respuesta de la busqueda
   *        content:
   *         application/json:
   *          schema:
   *            type: array
   *            items:
   *              type: string
   *               
   *   
   */
export const Search = (router) => {
    router.get('/api/search/:nombre', async(ctx) => {
            const res = await getMovies(ctx.params.nombre, ctx.request.headers.year);
            let final
            if(res.Response != "False"){
                final = new modelMovie({Title: res.Title, Year: res.Year, Released: res.Released, Genre: res.Genre, Director: res.Director, Actors: res.Actors, Plot: res.Plot, Ratings: res.Ratings });
                const result = await modelMovie.find({Title: res.Title});
                if(result.length === 0){
                    await final.save();
                    final = await modelMovie.find({Title: res.Title});
                }else{
                    final = {
                        Response: "False",
                        Error: "Duplicate data in db"
                    }
                }
            }else{
                final = res
            }
            
            ctx.body = final
    })
}

 /**
   * @swagger
   * /api/search/allmovies:
   *   get: 
   *    tags:
   *    - Search
   *    summary: Consulta por los todos los datos almacenados en la base de datos, paginadas de 5 elementos
   *    parameters:
   *     - in: header
   *       name: numPag
   *       description: numero de paginas a buscar
   *       schema:
   *         type: string
   *          
   *    responses:
   *       '200':
   *        description: un array JSON con la respuesta de la busqueda
   *        content:
   *         application/json:
   *          schema:
   *            type: array
   *            items:
   *              type: string
   *               
   *   
   */

export const SearchALLMovies = (router) => {
    router.get('/api/search/allmovies', async (ctx) => {
        try {
            const page = await ctx.request.headers.numpag;
            const res = await modelMovie.paginate({}, getPagination(page));
            ctx.body = {
               TotalItems: res.totalDocs,
               Movies: res.docs,
               TotalPages: res.totalPages,
               CurrentPage: res.page - 1
            }
        } catch (error) {
            console.log(error)
        }
        
    })

    
}

/**
   * @swagger
   * /api/setmovie/plot:
   *   post: 
   *    tags:
   *    - EditPlot
   *    summary: Cambia las palabras del plot de la pelicula seleccionada
   *    parameters:
   *      - in: body
   *        name: data
   *        schema:
   *          $ref: '#/definitions/BodyPlot'
   *         
   *              
   *    responses:
   *       '200':
   *        description: un array JSON con la respuesta de la busqueda
   *        content:
   *         application/json:
   *          schema:
   *            type: array
   *            items:
   *              type: string
   *         required: true
   *               
   *   
   */
export const EditPlot = (router) => {
    router.post('/api/setmovie/plot', async(ctx) => {
        try {
            const reqBody = await ctx.request.body;
            if(typeof reqBody.movie != "undefined" && typeof reqBody.find != "undefined" && typeof reqBody.replace != "undefined"){
                const title = await capitalizeFirstLetter(reqBody.movie)
                const mov = await modelMovie.find({Title: title })
                if(typeof mov[0] != "undefined"){
                    const plot = mov[0].Plot
                    const newPlot = changeWords(plot,reqBody.find,reqBody.replace)
                    ctx.body = {
                        Movie: title,
                        OldPlot: plot,
                        NewPlot: newPlot
                    }
                }else{
                    ctx.body = {
                        Response: "False",
                        Error: "Movie not found in db"
                    }
                }      
            }else{
                ctx.body = {
                    Response: "False",
                    Error: "Please insert a field: movie, find and replace"
                }
            }
        } catch (error) {
            ctx.body= {
                Response: "False",
                Error: error
            }
        }
    })
}