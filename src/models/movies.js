import {model, Schema} from 'mongoose'
import mongoosePaginate from 'mongoose-paginate-v2'
const modelMovie = new Schema({
    Title : {
        type: String,
        required: true
    },
    Year: {
        type: Number,
        required: true
    },
    Released: {
        type: String,
        required: true
    },
    Genre: {
        type: String,
        required: true
    },
    Director: {
        type: String,
        required: true
    },
    Actors: {
        type: String,
        required: true
    },
    Plot: {
        type: String,
        required: true
    },
    Ratings: [{
        Source: {
            type: String,
            required: true,
        },
        Value: {
            type: String,
            required: true
        }
    }],
}, {
    versionKey: false
})

modelMovie.plugin(mongoosePaginate);
export default model("movie", modelMovie)