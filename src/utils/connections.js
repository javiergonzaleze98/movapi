import axios from 'axios';
import config from "../config"

export const getMovies = async(title, year) => {
    if(typeof year != "undefined"){
        try{
          const data = await axios.get(`http://www.omdbapi.com/?apikey=${config.apikey}&t=${title}&y=${year}`).catch(error => console.log(error));
          if(data.data.Response != "False"){
              return {
              Title: data.data.Title,
              Year: data.data.Year,
              Released: data.data.Released,
              Genre: data.data.Genre,
              Director: data.data.Director,
              Actors: data.data.Actors,
              Plot: data.data.Plot,
              Ratings: data.data.Ratings 
            } 
          }else{
              return data.data
          }
          
        }catch(error){
            console.log(error)
        }
    }else{
        try{
            const data = await axios.get(`http://www.omdbapi.com/?apikey=${config.apikey}&t=${title}`).catch(error => console.log(error));
            if(data.data.Response != "False"){
                return {
                Title: data.data.Title,
                Year: data.data.Year,
                Released: data.data.Released,
                Genre: data.data.Genre,
                Director: data.data.Director,
                Actors: data.data.Actors,
                Plot: data.data.Plot,
                Ratings: data.data.Ratings 
             } 
            }else{
                return data.data
            }
            
          }catch(error){
              console.log(error)
          }
    }
}


export const getPagination = (pag) => {
    if(typeof pag != 'undefined'){
        const limit = 5;
        const offset = pag ? (pag * limit) : 0;
        return {limit, offset}
    }else{
        const limit = 5;
        const offset = 0;
        return {limit, offset}
    }
}