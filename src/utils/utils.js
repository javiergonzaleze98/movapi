export const capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
    }

export const toLowerCaseWord = (string) => {
    return string.charAt(0).toLowerCase() + string.slice(1);
    }

export const changeWords = (text, find, replace) => {
        const regexp = new RegExp(find, "g");
        const newPlot = text.replace(regexp, replace)
        return newPlot

    }