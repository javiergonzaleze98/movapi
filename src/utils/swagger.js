import Router from '@koa/router'// Importar función de enrutamiento
import swaggerJSDoc from 'swagger-jsdoc'
import path from 'path'

const router = new Router();

const swaggerDefinition = {
    info: {
        title: 'MovApi',
        version: '1.0.0',
        description: 'Esta API se encarga de almacenar datos de distintas peliculas en una base de datos en MongoDB para después consultarlas. Así como también se puede cambiar el Plot de las mismas.',
    },
    host: 'localhost:3000',
    basePath: '/' // Base path (optional)
};
const options = {
    swaggerDefinition,
    apis: [path.join(__dirname,'../routes/*.js')], // La dirección de almacenamiento del enrutador anotado, preferiblemente path.join()
};
const swaggerSpec = swaggerJSDoc(options)
 // Obtener el archivo de anotaciones generado mediante enrutamiento
router.get('/swagger.json', async function (ctx) {
    ctx.set('Content-Type', 'application/json');
    ctx.body = swaggerSpec;
})
export default router