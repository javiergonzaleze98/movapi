import mongoose from 'mongoose'
import config from './config'

(async () => {
   const db = await mongoose.connect(config.mongodbURL).catch(error => console.log(error))
   console.log("Base de datos conectada a: " + db.connection.name);
})();
