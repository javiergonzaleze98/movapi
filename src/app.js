import Koa from 'koa'
import {Search, SearchALLMovies, EditPlot} from './routes/search.routes'
import Router from '@koa/router'
import bodyParser from 'koa-bodyparser';
import logger from 'koa-logger';
import swagger from './utils/swagger'
import { koaSwagger } from 'koa2-swagger-ui';
const router = new Router();
const app = new Koa();
//Middlewares
app.use(swagger.routes(), swagger.allowedMethods())
app.use(koaSwagger({
    routePrefix: '/docs', // host at /swagger instead of default /docs
    swaggerOptions: {
      url: '/swagger.json'
    }, 
  }))
app.use(logger());
app.use(bodyParser());
app.use(async (ctx, next) => {
    ctx.set('Access-Control-Allow-Origin', '*');
    ctx.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    ctx.set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    await next();
  });

//Routes

SearchALLMovies(router)
Search(router)
EditPlot(router) 

app.use(router.routes())

export default app;